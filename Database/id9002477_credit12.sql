-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 20, 2019 at 04:22 PM
-- Server version: 10.3.13-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `id9002477_credit12`
--

-- --------------------------------------------------------

--
-- Table structure for table `transaction_log`
--

CREATE TABLE `transaction_log` (
  `id` int(50) NOT NULL,
  `sender_id` varchar(20) NOT NULL,
  `sender_name` varchar(20) NOT NULL,
  `reciever_id` varchar(20) NOT NULL,
  `reciever_name` varchar(20) NOT NULL,
  `amount_sent` float NOT NULL,
  `time` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaction_log`
--

INSERT INTO `transaction_log` (`id`, `sender_id`, `sender_name`, `reciever_id`, `reciever_name`, `amount_sent`, `time`) VALUES
(1, '2', 'Lucky', '1', 'Rohit', 500, 'Mar,20,2019 08:33:46 AM');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) NOT NULL,
  `name` varchar(10) NOT NULL,
  `email` varchar(50) NOT NULL,
  `contact_no` bigint(10) NOT NULL,
  `current_credit` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `contact_no`, `current_credit`) VALUES
(1, 'Rohit', 'rhtgarg01@gmail.com', 9000000000, 5724),
(2, 'Lucky', 'lkygarg2007@gmail.com', 7000000000, 291),
(3, 'Ram', 'ram123@gmail.com', 9041111111, 80),
(4, 'Uday', 'usand6@gmail.com', 7100000000, 1000),
(5, 'Shobhit', 'shobhit@gmail.com', 7800000000, 900),
(6, 'Charchit', 'charchit@gmail.com', 4000000000, 500),
(7, 'Shashank', 'shashank@gmail.com', 4500000000, 3600),
(8, 'Satyam', 'satyam@gmail.com', 7031245678, 1500),
(9, 'Sparsh', 'sparsh@gmail.com', 4678951230, 1105),
(10, 'Ajay', 'ajay@gmail.com', 1234567890, 1500);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `transaction_log`
--
ALTER TABLE `transaction_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `transaction_log`
--
ALTER TABLE `transaction_log`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
