<?php
session_start();
include "class1.php"
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Users-Credit Management System</title>

  <!-- Custom fonts for this template -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

  <!-- Custom styles for this page -->
  <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
</head>
<body>
	<div id="wrapper">

		<!-- Sidebar -->
		<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

		<!-- Sidebar - Brand -->
		<a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
			<div class="sidebar-brand-icon rotate-n-15">
				<i class="fas fa-laugh-wink"></i>
			</div>
			<div class="sidebar-brand-text mx-3">Credit Management System</div>
		</a>

		<!-- Divider -->
		<hr class="sidebar-divider my-0">

		<!-- Nav Item - Dashboard -->
		<li class="nav-item">
			<a class="nav-link" href="index.html">
				<i class="fas fa-fw fa-tachometer-alt"></i>
				<span>Home</span>
			</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" href="viewall.php">
				<i class="fas fa-fw fa-tachometer-alt"></i>
				<span>View All</span>
			</a>
		</li>
		</ul>
		<div id="content-wrapper" class="d-flex flex-column">

		<!-- Main Content -->
		<div id="content">
		<div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800" style="margin-top:20px;">Users-Credit</h1>
          <?php
            if(isset($_SESSION["success"])){
            $success=$_SESSION["success"];
            echo "<span style='color:blue;border:1px solid green;'>$success</span>";
            }
          ?>
		  <div class="card shadow mb-4">
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
				<thead>
					<tr>
						<td>ID</td>
						<td>Name</td>
						<td>Email</td>
						<td>Contact_No</td>
						<td>Current Credit</td>
						<td></td>
					</tr>
					</thead>
					<tbody>
					<?php
					$connec=new Connect();
					$connec->connect_fn();
					$sql= "SELECT * FROM users";
					$result = mysqli_query($connec->link, $sql);
						while ($row = $result->fetch_assoc()) {
							$f0=$row['id'];
							$f1=$row['name'];
							$f2=$row['email'];
							$f3=$row['contact_no'];
							$f4=$row['current_credit'];
						?>

					<tr>
						<td><?php echo $f0 ?></td>
						<td><?php echo $f1 ?></td>
						<td><?php echo $f2 ?></td>
						<td><?php echo $f3 ?></td>
						<td><?php echo $f4 ?></td>
						<!-- for viewing details of user -->
						<td><a href="/view.php?id=<?php echo $f0;?>">View Details</a></td>
					</tr>	
					<?php	}
					?>
					</tbody>
	</div>
	</div>
	</div>
	</div>
	</div>
</body>
</html>
<?php
unset($_SESSION["success"]);
?>
